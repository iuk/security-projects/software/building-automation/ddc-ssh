#!/bin/sh
echo "========================================================================"
echo "=               Kleine Zusammenfassung ueber das System                ="
echo "========================================================================"
echo "==================  Boot Environment ==================================="
fw_printenv
echo "==================  CPU ================================================"
cat /proc/cpuinfo
echo -n "CPU Hardware Rev: "
cat /proc/d4k_misc/revision

echo "==================  Flashkarten ========================================"
echo "HDA (vorne):"
echo "------------"
if [ -e /proc/ide/ide0/hda/model ]; then
   cat /proc/ide/ide0/hda/model
   cat /proc/ide/ide0/hda/geometry
else
   echo "Nicht gesteckt oder unbekannt"
fi
echo "HDB (Im Geraet):"
echo "----------------"
if [ -e /proc/ide/ide0/hdb/model ]; then
   cat /proc/ide/ide0/hdb/model
   cat /proc/ide/ide0/hdb/geometry
else
   echo "Nicht gesteckt oder unbekannt"
fi

echo "==================  Uptime  ============================================"
uptime
echo "==================  Interrupts  ========================================"
cat /proc/interrupts
echo "==================  Meminfo ============================================"
cat /proc/meminfo
echo "==================  Interprozesskommunikation =========================="
ipcs
echo "==================  Netzwerk ==========================================="
ifconfig
route
echo "==================  mounts ============================================="
cat /proc/mounts
echo "==================  commandline ========================================"
cat /proc/cmdline
echo "==================  Kernelversion ======================================"
cat /proc/version
echo "==================  Filesystem ========================================="
df
echo "=================== Systemzeit ========================================="
date
echo "=================== Sonstiges =========================================="
echo -n "Innentemperatur: "
cat /proc/d4k_misc/temp1
echo -n "USV Spannung: "
cat /proc/d4k_misc/voltage
echo -n "Batteriespannung :"
cat /proc/d4k_misc/battery
echo "=================== Kernelmeldungen ===================================="
dmesg
echo "=================== Calculate-Task maps & queues ======================="
Cgi-Interface -m
echo "=================== Liste der Objekte =================================="
Cgi-Interface -z
echo "=================== Can Statistik ======================================"
CanStat -sS
echo "=================== TouchPanel ========================================="
AAA=`cat /proc/d4k_misc/tpanel_msg_change_limit` > /dev/null
BBB=`cat /proc/d4k_misc/tpanel_data_change_limit` > /dev/null
CCC=`cat /proc/d4k_misc/tpanel_timer` > /dev/null
DDD=`cat /proc/d4k_misc/tpanel_beep` > /dev/null
echo "msg_change_limit=$AAA data_change_limit=$BBB timer=$CCC ms beep=$DDD"
echo "=================== Limits ============================================="
ulimit -a
echo "=================== ENDE ==============================================="

