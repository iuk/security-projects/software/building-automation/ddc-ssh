
#!/bin/sh
#
# Speicherueberwachung zu Debugzwecken
#

# 1.5MB unterhalb 1.xMB keine Chance mehr zu reagieren
# nicht besonders ingenieursmaessig
MEM_FREE_LIMIT=1536

#logger "paranoia gestartet - in 2 min startet die Ueberwachung"
#sleep 120

# paranoia laeuft nur, wenn nix anderes anliegt
#renice 20 $$

logEmergencyMessage()
{
  logger -p emerg "$1"
  logInfoMessage "$1"
}

logInfoMessage()
{
  logger -p info "$1"
  echo "$1" 1>&2
}

while [ 1 ]; do
  tmp=`cat /proc/meminfo | grep MemFree`
  MemFree=`echo $tmp | cut -f2 -d ' '`
  if [ "$MEM_FREE_LIMIT" -ge "$MemFree" ] ; then
    logEmergencyMessage "paranoia: Achtung, der Speicher wird knapp: $MemFree kB sind noch frei"
    MaxVmSize=0
    statusFiles=`find /proc -name status`
    for file in $statusFiles
    do
      if [ -e "$file" ] # wir testen, ob das status-File noch da ist, da mindestens find bereits terminiert hat
      then
        #echo $file
        pid=`echo $file | cut -f3 -d '/'`
        #echo $pid
        tmp=`grep "Name:" /proc/$pid/status`
        Name=`echo $tmp | cut -f2 -d ' '`
        tmp=`grep "VmSize:" /proc/$pid/status`
        VmSize=`echo $tmp | cut -f2 -d ' '`
        logEmergencyMessage "paranoia: Name: $Name Pid: $pid VmSize: $VmSize"
        if [ "$VmSize" -gt "$MaxVmSize" ] ; then
          MaxVmSize=$VmSize
          KillKandidatePid=$pid
          KillKandidateName=$Name
        fi
      fi
    done
    # autsch: nun killen wir den schlimmsten von Allen
    logEmergencyMessage "paranoia: kill $KillKandidateName $KillKandidatePid"
    kill $KillKandidatePid
    sleep 1
    kill -9 $KillKandidatePid
  fi
  sleep 1
done

