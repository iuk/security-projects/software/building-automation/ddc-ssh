#!/bin/sh

# ============================================================================
# $Id: d4run.sh,v 1.5 2007/10/16 12:43:47 heydeck Exp $
#
# Copyright (c) Kieback & Peter GmbH u. Co KG, 2003.
# All Rights Reserved.
#
# Author: Ullrich Meyer
# ============================================================================

#
# D4 Startskript fuer das Starten von Applikationen auf der Zielhardware
# d4run.sh ist dafuer vorgesehen alle Applikationen, die nicht von DwhTask erben
# zu starten, damit diese einer Ueberwachung untergeordnet werden koennen.
# d4run.sh liest /etc/d4run.cfg, setzt die fuer die zu startende Applikation darin
# angegebenen Ressourcelimits und prueft, ob schon die maximale Anzahl
# Instanzen der Applikation laeuft.
#
# Die Ueberwachung erfolgt durch die Startup-Task.
# Programme registrieren sich zur Ueberwachung,
# indem sie ihre Startzeile in ein Datei /tmp/d4run_PID.register schreibt.
# Die Startup-Task sucht zyklisch in /tmp nach diesen Dateien,
# liest deren Inhalt und loescht sie, sobald die Datein in die
# d4run-Registry der Startup-Task eingetragen ist.
# Um ein Programm aus der d4run-Registry der Startup-Task zu entfernen,
# ist eine Datei /tmp/d4run_PID.unregister anzulegen.
# Diese Datei wird von der Startup-Task nach der Deregistrierung des Programms
# aus der d4run-Registry durch die Startup-Task entfernt.
# Die Deregistrierung wird seitens der Startup-Task durch das Anlegen einer
# Datei /tmp/d4run_PID.unregistered angezeigt, diese ist seitens
# des aufrufenden Programms zu entfernen.
#
# Etwas komplizierter ist die Behandlung vom Daemons.
# Es knann sich als notwendig erweisen, diese bewusst zu beenden,
# und mit neuen Argumenten erneut zu starten.
#
#

# Achtung, PROGRAMM enthaelt unueblicherweise das via d4run.sh zu startende Programm, nicht $0 !
#echo "$@"
PROGRAM=`basename $1`
#echo "$PROGRAM"
#echo "$@"
ERROREXIT=1
OKEXIT=0


#
# Gibt Hilfe-Information aus und beendet das Programm
#
usage()
{
    echo "
Aufruf: d4run.sh [-stop pid] Argumente

Start- und Stop-Skript fuer \"Nicht-Dwh_Tasks\".

Optionen:
    -stop pid Stoppend einen Daemon, der mittel d4run.sh gestartet wurde
    "
    exit $OKEXIT
}

HARDWARE=`uname -m`
if [ $HARDWARE = "ppc" ]; then
    CFGFILE="/etc/d4run.cfg"
else
    CFGPATH=`dirname $0`
    CFGFILE="$CFGPATH/d4run.cfg"
fi

#
# Schreibt eine Info-Meldung nach syslog
#
loginfo()
{
    logger -p local6.info "$1"
}

#
# Schreibt eine Error-Meldung nach syslog und stdout
#
logerror()
{
    echo "$1"
    logger -p local5.err "$1"
}

#
# Fuehrt einen Warmstart der DDC4000 aus
#
warmstart()
{
    logerror "Warmstart ausgefuehrt: $1"
    # !!!umy noch impl.
}

#
# Fuehrt einen Reset der DDC4000 aus
#
reset()
{
    logerror "Reset ausgefuehrt: $1"
    # !!!umy noch impl.
}

#
# Fuehrt einen Neustart der DDC4000 aus
# $1 .. "warm" oder "reset"
#
restart()
{
    if [ "$1" = "warm" ]; then
        warmstart "$2"
    else
        reset "$2"
    fi
}

#
# Deregistriert ein Programm aus der d4run-Registry der Startup-Task
#
# Argumente:
# $1 .. pid
#
deregister()
{
    unregisterFile="/tmp/d4run_$1.unregister"
    loginfo "d4run unregister: unregisterFile=$unregisterFile"
    echo "done" > $unregisterFile
    sleep 2
    unregisteredFile="/tmp/d4run_$1.unregistered"
    while [ ! -f "$unregisteredFile" ]
    do
        loginfo "d4run unregister: cmd='$cmd', warte auf unregisteredFile=$unregisteredFile"
        sleep 1
    done
    rm -f "$unregisteredFile"
}

#
# Startet eine Applikation unter Beachtung der Angaben in $CFGFILE
#
# Parameter:
# $@ .. Argumente
#
# sowie die bereits ermittelten Variablen:
# $progname
# $appltype
# $ninst
# $rlimit_as
# $rlimit_core
# $rlimit_cpu
# $rlimit_data
# $rlimit_fsize
# $rlimit_memlock
# $rlimit_nofile
# $rlimit_nproc
# $rlimit_stack
# $onerr
# $insterr
#
run()
{
    #echo "run: $@"
    # ulimit
    # -a     All current limits are reported
    # -c     The maximum size of core files created
    # -d     The maximum size of a process's data segment
    # -f     The maximum size of files created by the shell
    # -l     The maximum size that may be locked into memory
    # -m     The maximum resident set size
    # -n     The  maximum number of open file descriptors (most systems do not allow this value to
    #        be set)
    # -p     The pipe size in 512-byte blocks (this may not be set)
    # -s     The maximum stack size
    # -t     The maximum amount of cpu time in seconds
    # -u     The maximum number of processes available to a single user
    # -v     The maximum amount of virtual memory available to the shell

    ulimit_opts=""
    if [ $rlimit_as -gt 0 ]; then
        ulimit_opts="$ulimit_opts -v $rlimit_as"
    fi
    if [ $rlimit_core -gt 0 ]; then
        ulimit_opts="$ulimit_opts -c $rlimit_core"
    fi
    if [ $rlimit_cpu -gt 0 ]; then
        ulimit_opts="$ulimit_opts -t $rlimit_cpu"
    fi
    if [ $rlimit_data -gt 0 ]; then
        ulimit_opts="$ulimit_opts -d $rlimit_data"
    fi
    if [ $rlimit_fsize -gt 0 ]; then
        ulimit_opts="$ulimit_opts -f $rlimit_fsize"
    fi
    if [ $rlimit_memlock -gt 0 ]; then
        ulimit_opts="$ulimit_opts -l $rlimit_memlock"
    fi
    if [ $rlimit_nofile -gt 0 ]; then
        ulimit_opts="$ulimit_opts -n $rlimit_nofile"
    fi
    if [ $rlimit_nproc -gt 0 ]; then
        ulimit_opts="$ulimit_opts -u $rlimit_nproc"
    fi
    if [ $rlimit_stack -gt 0 ]; then
        ulimit_opts="$ulimit_opts -s $rlimit_stack"
    fi
    if [ "$ulimit_opts" != "" ]; then
        #echo "ulimit $ulimit_opts"
        ulimit $ulimit_opts
        #ulimit -a
    fi
    cmd="$@"
    #echo "cmd='$cmd'"
    if [ "$appltype" = "daem" ]; then
        # starte cmd als Daemon
        #echo "daem: cmd='$cmd'"
        ${cmd} &
        #echo "pid of $progname= $! ret=$ret"
        # ueberlasse die Ueberwachung der ueberwachenden Applikation
        # Problem: Rueckgabewert geht verloren
        registerFile="/tmp/d4run_$!.register"
        loginfo "d4run starte Daemon: cmd='$cmd', registerFile=$registerFile"
        echo "$@" > $registerFile
    else
        if [ "$appltype" = "appl" ]; then
            # trage mich selbst zur Ueberwachung bei der ueberwachenden Applikation ein
            registerFile="/tmp/d4run_$$.register"
            loginfo "d4run starte Applikation: cmd='$cmd', registerFile=$registerFile"
            echo "$0 $@" > $registerFile
            outp=`$cmd`
            ret=$?
            loginfo "d4run Applikation beendet: cmd='$cmd', registerFile=$registerFile, ret='$ret'"
            if [ $ret != 0 ]; then
                if [ "$onerr" = "reexec" ]; then
                    while [ $ret != 0 ]
                    do
                        logerror "d4run Fehler beim Ausfuehren einer Applikation: cmd='$cmd', Rueckgabewert=$ret', starte erneut ..."
                        outp=`$cmd`
                        ret=$?
                    done
                else
                    restart $onerr "d4run Fehler beim Ausfuehren von '$cmd', Rueckgabewert=$ret"
                fi
            fi
            deregister "$$"
        else
            logerror "Falscher Applikationstyp fuer $PROGRAM in $CFGFILE: $appltype"
            exit $ERROREXIT
        fi
    fi
}

#
# Inhalt d4run.cfg
# program   appl.-type  ninst   rlas	rlcore	rlcpu	rldata	rlfsize	rlmlock rlnfile rlnproc rlstack onerr   insterr
#

#progname=""
#appltype=""
#ninst=0
#rlimit_as=0
#rlimit_core=0
#rlimit_cpu=0
#rlimit_data=0
#rlimit_fsize=0
#rlimit_memlock=0
#rlimit_nofile=0
#rlimit_nproc=0
#rlimit_stack=0
#onerr=""
#insterr=""

stopDaemon=""
if [ "$1" = "" ];then
    usage
else
    if [ "$1" = "-stop" ]; then
        if [ "$2" = "" -o ! -f "/proc/$2/status" ]; then
            usage
        else
            stopDaemon="$2"
        fi
    fi
fi

if [ "$stopDaemon" != "" ]; then
    deregister "$stopDaemon"
    kill -2 "$stopDaemon"
    sleep 5
    if [ -f "/proc/$stopDaemon/status" ]; then
        kill "$stopDaemon"
        sleep 5
    fi
    if [ -f "/proc/$stopDaemon/status" ]; then
        logerror "d4run -stop, Daemon konnte nicht beendet werden, pid=$stopDaemon"
        exit $ERROREXIT
    else
        exit $OKEXIT
    fi
fi

cfg=`grep $PROGRAM $CFGFILE`
if [ "$cfg" = "" ]; then
    logerror "$PROGRAM nicht in $CFGFILE enhalten"
    exit $ERROREXIT
fi

#echo $cfg
progname=`echo $cfg | cut -d ' ' -f 1`
appltype=`echo $cfg | cut -d ' ' -f 2`
ninst=`echo $cfg | cut -d ' ' -f 3`
rlimit_as=`echo $cfg | cut -d ' ' -f 4`
rlimit_core=`echo $cfg | cut -d ' ' -f 5`
rlimit_cpu=`echo $cfg | cut -d ' ' -f 6`
rlimit_data=`echo $cfg | cut -d ' ' -f 7`
rlimit_fsize=`echo $cfg | cut -d ' ' -f 8`
rlimit_memlock=`echo $cfg | cut -d ' ' -f 9`
rlimit_nofile=`echo $cfg | cut -d ' ' -f 10`
rlimit_nproc=`echo $cfg | cut -d ' ' -f 11`
rlimit_stack=`echo $cfg | cut -d ' ' -f 12`
onerr=`echo $cfg | cut -d ' ' -f 13`
insterr=`echo $cfg | cut -d ' ' -f 14`
#echo "in d4run.cfg: $progname $appltype $ninst $rlimit_as $rlimit_core $rlimit_cpu $rlimit_data $rlimit_fsize $rlimit_memlock $rlimit_nofile $rlimit_nproc $rlimit_stack $onerr $insterr $waitto $waiterr"

inst=`pidof $PROGRAM | wc -w`
#echo "inst='$inst'"
#echo "ninst='$ninst'"

if [ "$ninst" != "0" ]; then 
    if [ $inst -ge $ninst ]; then
        if [ "$insterr" = "wait" ]; then
            waitto=`echo $cfg | cut -d ' ' -f 15`
            waiterr=`echo $cfg | cut -d ' ' -f 16`
            Secs=0
            #warten
            loginfo "d4run: warte $waitto s auf Beendigung von $PROGRAM"
            while [ "$Secs" -lt "$waitto" ]
            do
                inst=`pidof $PROGRAM | wc -w`
                if [ $inst -ge $ninst ]; then
                    sleep 1
                    let "Secs += 1"
                else
                    break
                fi
           done
            inst=`pidof $PROGRAM | wc -w`
            if [ $inst -ge $ninst ]; then
                restart $waiterr "$PROGRAM nicht rechtzeitig beendet"
            fi
        else
            restart $insterr "$PROGRAM max. Anzahl Instanzen ueberschritten"
        fi
    fi
fi

run $@

