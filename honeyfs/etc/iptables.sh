#!/bin/sh


# Aus  http://neonics.net/sirius/stuff/firewall.txt  Hey, 4.5.2011

echo "iptables.sh"

case "$1" in

    restart)
	iptables -F

#   icmp - flood:
	iptables -N icmp-flood
	iptables -A INPUT -p icmp -i eth0 -j icmp-flood
	iptables -A icmp-flood -i eth0 -m limit --limit 2/s --limit-burst 4 -j RETURN
	iptables -A icmp-flood -i eth0 -j DROP
    ;;

    *)
	/bin/echo "Usage: $0 {restart}"
    ;;
esac
